package com.adamkorzeniak.myapp.examples.notification;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.adamkorzeniak.myapp.R;
import com.adamkorzeniak.myapp.common.android.notification.ToastService;

import java.util.concurrent.atomic.AtomicReference;

//TODO: Finish and refactor whole package and xml file
public class NotificationExampleActivity extends AppCompatActivity {

    private final ToastService toastService;

    public NotificationExampleActivity() {
        this.toastService = ToastService.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_example);

        AlertDialog.Builder alert1 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setMessage("Now you know how to use a new component!")
                .setIcon(android.R.drawable.ic_delete)
                .setPositiveButton(android.R.string.ok, (a, b) ->
                        Toast.makeText(this, "Ok", Toast.LENGTH_SHORT).show())
                .setNegativeButton(android.R.string.cancel, null);


        String[] items = {"First Item", "Second Item", "Third Item"};
        AlertDialog.Builder alert2 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setItems(items, (dialog, which) -> Toast.makeText(this, items[which] + " is selected", Toast.LENGTH_SHORT)
                        .show());


        String[] choice = {"First Item", "Second Item", "Third Item"};
        AtomicReference<String> chosen = new AtomicReference<>();
        AlertDialog.Builder alert3 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setSingleChoiceItems(choice, -1, (dialog, which) -> chosen.set(choice[which]))
                .setPositiveButton(android.R.string.ok, (a, b) ->
                        Toast.makeText(this, chosen.get(), Toast.LENGTH_SHORT).show());


        findViewById(R.id.button1).setOnClickListener(v -> alert1.show());
        findViewById(R.id.button2).setOnClickListener(v -> alert2.show());
        findViewById(R.id.button3).setOnClickListener(v -> alert3.show());
        findViewById(R.id.toast).setOnClickListener(v -> toastService.showToast(this, "Dependency Injection succeded"));
    }
}