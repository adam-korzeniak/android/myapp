package com.adamkorzeniak.myapp.common.android.notification;

import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;

public class NotificationChannelGroupBuilder {

    private final Context context;
    private final String id;
    private final String name;

    public NotificationChannelGroupBuilder(Context context, String id, String name) {
        this.context = context;
        this.id = id;
        this.name = name;
    }

    public void init() {
        NotificationChannelGroup notificationChannelGroup = new NotificationChannelGroup(id, name);
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannelGroup(notificationChannelGroup);
    }

}
