package com.adamkorzeniak.myapp.common.android.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

public class NotificationChannelBuilder {

    private final Context context;
    private final String id;
    private final String name;
    private String description;
    private String groupId;
    private int importance = NotificationManager.IMPORTANCE_DEFAULT;

    public NotificationChannelBuilder(Context context, String id, String name) {
        this.context = context;
        this.id = id;
        this.name = name;
    }

    public NotificationChannelBuilder description(String description) {
        this.description = description;
        return this;
    }

    public NotificationChannelBuilder importance(int importance) {
        this.importance = importance;
        return this;
    }

    public NotificationChannelBuilder group(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public void init() {
        NotificationChannel channel = new NotificationChannel(id, name, importance);
        channel.setDescription(description);
        channel.setGroup(groupId);
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }
}
