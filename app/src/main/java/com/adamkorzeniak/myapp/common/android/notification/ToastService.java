package com.adamkorzeniak.myapp.common.android.notification;

import android.content.Context;
import android.widget.Toast;

public class ToastService {

    private static ToastService instance;

    public ToastService() {
    }

    public static synchronized ToastService getInstance() {
        if (instance == null) {
            instance = new ToastService();
        }
        return instance;
    }

    public void showToast(Context context, String text) {
        showShortToast(context, text);
    }

    public void showShortToast(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showLongToast(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }
}
