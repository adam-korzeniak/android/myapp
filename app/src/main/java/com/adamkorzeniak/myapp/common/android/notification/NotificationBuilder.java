package com.adamkorzeniak.myapp.common.android.notification;

import android.app.Notification;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import com.adamkorzeniak.myapp.R;

public class NotificationBuilder {

    private final Context context;
    private final String channelId;
    private String title;
    private String text;
    private int priority = NotificationCompat.PRIORITY_DEFAULT;
    private int icon = R.drawable.default_notification_icon;

    public NotificationBuilder(Context context, String channelId) {
        this.context = context;
        this.channelId = channelId;
    }

    public NotificationBuilder title(String title) {
        this.title = title;
        return this;
    }

    public NotificationBuilder text(String text) {
        this.text = text;
        return this;
    }

    public NotificationBuilder icon(int icon) {
        this.icon = icon;
        return this;
    }

    public NotificationBuilder priority(int priority) {
        this.priority = priority;
        return this;
    }

    public Notification build() {
        return new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(priority)
                .setAutoCancel(true)
                .build();
    }
}
