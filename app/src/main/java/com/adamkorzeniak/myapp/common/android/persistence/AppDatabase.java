package com.adamkorzeniak.myapp.common.android.persistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.adamkorzeniak.myapp.crypto.CryptoAssetDao;
import com.adamkorzeniak.myapp.crypto.CryptoAssetEntity;

@Database(entities = {CryptoAssetEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "myDatabase";

    public abstract CryptoAssetDao getCryptoAssetDao();
}
