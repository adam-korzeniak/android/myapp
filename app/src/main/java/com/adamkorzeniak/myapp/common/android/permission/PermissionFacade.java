package com.adamkorzeniak.myapp.common.android.permission;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class PermissionFacade {

    private static final Map<String, Integer> PERMISSION_REQUEST_CODES = new HashMap<>();
    private static final Random RANDOM = new Random();
    private static PermissionFacade instance;

    public PermissionFacade() {
    }

    public static PermissionFacade getInstance() {
        if (instance == null) {
            instance = new PermissionFacade();
        }
        return instance;
    }

    public void requestPermissions(Activity activity, Collection<String> permissions) {
        Optional.ofNullable(permissions).orElseGet(Collections::emptyList)
                .forEach(permission -> requestPermission(activity, permission));
    }

    public void requestPermission(Activity activity, String permission) {
        Integer requestCode = PERMISSION_REQUEST_CODES.computeIfAbsent(permission, k -> RANDOM.nextInt(1000000));
        if (!hasPermission(activity, permission)) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
        }
    }


    public boolean hasPermission(Context activity, String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }
}
