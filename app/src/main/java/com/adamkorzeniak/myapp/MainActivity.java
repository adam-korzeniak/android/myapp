package com.adamkorzeniak.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.adamkorzeniak.myapp.examples.notification.NotificationExampleActivity;
import com.adamkorzeniak.myapp.goals.fitness.FitnessTrackingActivity;
import com.adamkorzeniak.myapp.teide.ui.TeideActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setTitle();
        setupButtons();
    }

    private void setTitle() {
        String title = getString(R.string.main_activity_title);
        setTitle(title);
    }

    private void setupButtons() {
        Button teide = findViewById(R.id.teide);
        teide.setOnClickListener(v -> goToActivity(TeideActivity.class));

        Button persistenceExample = findViewById(R.id.fitness_tracking);
        persistenceExample.setOnClickListener(v -> goToActivity(FitnessTrackingActivity.class));

        Button notificationExample = findViewById(R.id.notification_example);
        notificationExample.setOnClickListener(v -> goToActivity(NotificationExampleActivity.class));
    }

    private void goToActivity(Class<? extends AppCompatActivity> activity) {
        final Intent intent = new Intent(this, activity);
        startActivity(intent);
    }
}