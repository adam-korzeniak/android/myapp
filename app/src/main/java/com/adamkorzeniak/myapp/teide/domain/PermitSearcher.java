package com.adamkorzeniak.myapp.teide.domain;

import android.Manifest;
import android.app.Notification;
import android.content.Context;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.adamkorzeniak.myapp.R;
import com.adamkorzeniak.myapp.common.android.notification.NotificationBuilder;
import com.adamkorzeniak.myapp.common.android.permission.PermissionFacade;
import com.adamkorzeniak.myapp.teide.ui.GlobalData;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;

public class PermitSearcher implements Callable<Boolean> {

    private final WebClient webClient;
    private final PermissionFacade permissionFacade;
    private final Context context;

    public PermitSearcher(Context context) {
        this.webClient = buildWebClient();
        this.permissionFacade = PermissionFacade.getInstance();
        this.context = context;
    }

    private static HtmlPage proceedToBookingsPage(HtmlPage page) {
        DomElement nextPageButton = page.getElementById(Config.InitialPage.RESERVATION_BUTTON_ID);
        if (nextPageButton == null) {
            throw new RuntimeException("No button on Initial Page");
        }
        try {
            return nextPageButton.click();
        } catch (IOException e) {
            throw new RuntimeException("Failed to proceed to Booking Page", e);
        }
    }

    private static CalendarPage proceedToCalendarPage(HtmlPage bookingsPage) {
        DomElement nextPageButton = bookingsPage.getElementById(Config.BookingPage.TO_CALENDAR_BUTTON);
        if (nextPageButton == null) {
            throw new RuntimeException("No button on Booking Page");
        }
        HtmlPage calendarPage;
        try {
            calendarPage = nextPageButton.click();
        } catch (IOException e) {
            throw new RuntimeException("Failed to proceed to Calendar Page", e);
        }
        return new CalendarPage(calendarPage);
    }

    private static WebClient buildWebClient() {
        WebClient webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setCssEnabled(false);
        return webClient;
    }

    public boolean processSearch() {
        HtmlPage initialPage = getInitialPage();
        HtmlPage bookingsPage = proceedToBookingsPage(initialPage);
        CalendarPage calendarPage = proceedToCalendarPage(bookingsPage);
        return calendarPage.hasAnyValidDate();
    }

    private HtmlPage getInitialPage() {
        try {
            return webClient.getPage(Config.InitialPage.INITIAL_PAGE_URL);
        } catch (IOException e) {
            throw new RuntimeException("Loading Initial Page Failed", e);
        }
    }

    @Override
    public Boolean call() {
        markProcessStarted();

        boolean success = processSearch();
        if (success) {
            showSuccessNotification(context);
        }

        markProcessFinished();
        return success;
    }

    private void showSuccessNotification(Context context) {
        String channelId = context.getString(R.string.teide_channel_id);
        Notification notification = new NotificationBuilder(context, channelId)
                .title("El Teide")
                .text("Permit for El teide avaliable")
                .priority(NotificationCompat.PRIORITY_MAX)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        if (permissionFacade.hasPermission(context, Manifest.permission.POST_NOTIFICATIONS)) {
            notificationManager.notify(123, notification);
        }
    }

    public static void markProcessStarted() {
        GlobalData.updateProcessStarted();
    }

    public static void markProcessFinished() {
        GlobalData.updateProcessFinished();
    }
}
