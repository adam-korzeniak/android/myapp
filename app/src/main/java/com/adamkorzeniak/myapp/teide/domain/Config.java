package com.adamkorzeniak.myapp.teide.domain;

import static com.adamkorzeniak.myapp.teide.domain.Config.CalendarPage.SUPPORTED_MONTH_TO_DAYS;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

class Config {

    private Config() {}

    private static boolean debugEnabled = false;

    public static void changeDebugMode(boolean debugEnabled) {
        Config.debugEnabled = debugEnabled;
        if (debugEnabled) {
            SUPPORTED_MONTH_TO_DAYS.put("septiembre de 2023", List.of(1, 2, 3, 4, 15, 16, 17, 18, 19, 29));
        } else {
            SUPPORTED_MONTH_TO_DAYS.remove("septiembre de 2023");
        }

    }

    public static boolean isDebugEnabled() {
        return debugEnabled;
    }

    public static class Schedule {
        private Schedule() {}
        public static final int MIN_DELAY = 1000 * 60 * 5;
        public static final int MAX_DELAY = 1000 * 60 * 10;

        public static int getMinDelay() {
            if (debugEnabled) {
                return 1000 * 10;
            }
            return MIN_DELAY;
        }

        public static int getMaxDelay() {
            if (debugEnabled) {
                return 1000 * 20;
            }
            return MAX_DELAY;
        }
    }

    public static class InitialPage {
        private InitialPage() {}

        public static final String INITIAL_PAGE_URL = "https://www.reservasparquesnacionales.es/real/ParquesNac/usu/html/detalle-actividad-oapn.aspx?cen=2&act=1";
        public static final String RESERVATION_BUTTON_ID = "Button1";
    }

    public static class BookingPage {
        private BookingPage() {}


        public static final String TO_CALENDAR_BUTTON = "Button1";
    }

    public static class CalendarPage {
        private CalendarPage() {}
        public static final String CALENDAR_ID = "Calendar1";
        public static final String CALENDAR_HEADER_XPATH = "//table[contains(@class, 'meses')]";
        public static final String DAYS_XPATH = "//td[contains(@class, 'dias')]";

        public static final SortedMap<String, List<Integer>> SUPPORTED_MONTH_TO_DAYS = new TreeMap<>();

        static {
            SUPPORTED_MONTH_TO_DAYS.put("julio de 2023", List.of(29, 30, 31));
            SUPPORTED_MONTH_TO_DAYS.put("agosto de 2023", List.of(1, 2, 3, 4));
            if (debugEnabled) {
                SUPPORTED_MONTH_TO_DAYS.put("septiembre de 2023", List.of(1, 2, 3, 4, 15, 16, 17));
            }
        }
    }

}
