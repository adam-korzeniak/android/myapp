package com.adamkorzeniak.myapp.teide.ui;

import static android.Manifest.permission;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.adamkorzeniak.myapp.R;
import com.adamkorzeniak.myapp.common.android.notification.NotificationChannelBuilder;
import com.adamkorzeniak.myapp.common.android.notification.NotificationChannelGroupBuilder;
import com.adamkorzeniak.myapp.common.android.notification.ToastService;
import com.adamkorzeniak.myapp.common.android.permission.PermissionFacade;
import com.adamkorzeniak.myapp.teide.domain.SearchPermitFacade;

import java.time.LocalTime;
import java.util.Set;

//TODO: Refactor activity and domain
public class TeideActivity extends AppCompatActivity {

    private final SearchPermitFacade searchPermitFacade;
    private final PermissionFacade permissionFacade;

    public TeideActivity() {
        this.searchPermitFacade = SearchPermitFacade.getInstance();
        this.permissionFacade = PermissionFacade.getInstance();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teide);

        setTitle();
        checkPermissions();
        initializeNotificationsChannels();

        setupButtons();
    }

    private void setTitle() {
        String title = getString(R.string.teide_activity_title);
        setTitle(title);
    }

    public void checkPermissions() {
        Set<String> requiredPermissions = Set.of(permission.POST_NOTIFICATIONS, permission.INTERNET);
        permissionFacade.requestPermissions(this, requiredPermissions);
    }

    private void initializeNotificationsChannels() {
        String teideChannelGroupId = getString(R.string.teide_channel_group_id);
        String teideChannelGroupName = getString(R.string.teide_channel_group_name);
        new NotificationChannelGroupBuilder(this, teideChannelGroupId, teideChannelGroupName).init();

        String teideChannelId = getString(R.string.teide_channel_id);
        String teideChannelName = getString(R.string.teide_channel_name);
        String teideDebugChannelId = getString(R.string.teide_debug_channel_id);
        String teideDebugChannelName = getString(R.string.teide_debug_channel_name);
        new NotificationChannelBuilder(this, teideChannelId, teideChannelName)
                .group(teideChannelGroupId)
                .init();
        new NotificationChannelBuilder(this, teideDebugChannelId, teideDebugChannelName)
                .group(teideChannelGroupId)
                .init();
    }

    private void setupButtons() {
        Button searchButton = findViewById(R.id.search_button);
        searchButton.setOnClickListener(v -> searchPermitFacade.search(this));

        Button searchWithDebugButton = findViewById(R.id.debug_search_button);
        searchWithDebugButton.setOnClickListener(v -> searchPermitFacade.searchWithDebug(this));

        Button refreshDataButton = findViewById(R.id.refresh_data);
        refreshDataButton.setOnClickListener(v -> refreshData());
    }

    private void refreshData() {
        TextView processStarted = findViewById(R.id.process_started);
        String processStartedText = getString(R.string.process_started_text);
        processStarted.setText(String.format(processStartedText + ": %s", toString(GlobalData.getLastProcessStarted())));

        TextView processEnded = findViewById(R.id.process_finished);
        String processEndedText = getString(R.string.process_ended_text);
        processEnded.setText(String.format(processEndedText + ": %s", toString(GlobalData.getLastProcessFinished())));
    }

    private static String toString(LocalTime time) {
        if (time == null) {
            return "";
        }
        return time.toString();
    }
}