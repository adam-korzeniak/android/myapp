package com.adamkorzeniak.myapp.teide.domain;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class CalendarPage {
    private HtmlPage page;
    private DomElement calendar;
    private String monthText;
    private HtmlTableCell nextMonth;

    public CalendarPage(HtmlPage page) {
        this.page = page;
        updateCalendarDetails();
    }

    private void updateCalendarDetails() {
        calendar = page.getElementById(Config.CalendarPage.CALENDAR_ID);

        HtmlTable headersTable = (HtmlTable) calendar.getByXPath(Config.CalendarPage.CALENDAR_HEADER_XPATH).get(0);

        HtmlTableCell month = headersTable.getCellAt(0, 1);
        monthText = month.getTextContent();
        nextMonth = headersTable.getCellAt(0, 2);
    }

    public boolean isSupportedMonth() {
        return Config.CalendarPage.SUPPORTED_MONTH_TO_DAYS.keySet().stream()
                .anyMatch(text -> monthText.contains(text));
    }

    public boolean hasAnyValidDate() {
        for (int i = 0; i < 5; i++) {
            if (isSupportedMonth() && isAnyDayForMonthSupported()) {
                return true;
            }

            if (isFinalMonth()) {
                return false;
            }

            toNextPage();

        }
        return false;
    }

    public boolean isAnyDayForMonthSupported() {
        List<Object> supportedDays = calendar.getByXPath(Config.CalendarPage.DAYS_XPATH).stream()
                .filter(day -> isDaySupported((HtmlTableCell) day))
                .collect(Collectors.toList());
        return !supportedDays.isEmpty();
    }

    private boolean isDaySupported(HtmlTableCell day) {
        boolean daySupported = Config.CalendarPage.SUPPORTED_MONTH_TO_DAYS.getOrDefault(monthText, Collections.emptyList())
                .contains(Integer.parseInt(day.getTextContent().trim()));
        String backgroundColor = day.getStyleElement("background-color").getValue();
        return daySupported && !backgroundColor.contains("Gray");
    }

    public void toNextPage() {
        try {
            DomElement firstElementChild = nextMonth.getFirstElementChild();
            page = firstElementChild.click();
            updateCalendarDetails();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isFinalMonth() {
        return monthText.contains(Config.CalendarPage.SUPPORTED_MONTH_TO_DAYS.lastKey());
    }
}
