package com.adamkorzeniak.myapp.teide.ui;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

import com.adamkorzeniak.myapp.teide.domain.PermitSearcher;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SearchTeidePermitWorker extends ListenableWorker {


    public SearchTeidePermitWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        PermitSearcher searcher = new PermitSearcher(getApplicationContext());
        FutureTask<Boolean> futureTask = new FutureTask<>(searcher);
        Thread t = new Thread(futureTask);
        t.start();

        return new ListenableFuture() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public Object get() throws ExecutionException, InterruptedException {
                return null;
            }

            @Override
            public Object get(long timeout, TimeUnit unit) throws ExecutionException, InterruptedException, TimeoutException {
                return null;
            }

            @Override
            public void addListener(Runnable listener, Executor executor) {

            }
        };
    }
}
