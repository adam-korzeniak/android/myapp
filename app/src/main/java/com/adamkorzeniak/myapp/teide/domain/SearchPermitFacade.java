package com.adamkorzeniak.myapp.teide.domain;

import android.app.Activity;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.adamkorzeniak.myapp.teide.ui.SearchTeidePermitWorker;

import java.util.concurrent.TimeUnit;

//TODO: Refactor package
public class SearchPermitFacade {

    private static SearchPermitFacade instance;

    public static SearchPermitFacade getInstance() {
        if (instance == null) {
            instance = new SearchPermitFacade();
        }
        return instance;
    }

    public void search(Activity activity) {
        search(activity, false);
    }

    public void searchWithDebug(Activity activity) {
        search(activity, true);
    }

    private void search(Activity activity, boolean debugMode) {
        Config.changeDebugMode(debugMode);
        startSearch(activity);
    }

    private void startSearch(Activity activity) {
        WorkManager workManager = WorkManager.getInstance(activity);

        PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(SearchTeidePermitWorker.class, 15, TimeUnit.MINUTES)
                .build();

        workManager.enqueueUniquePeriodicWork(
                "TEIDE_PERMIT_SEARCH",
                ExistingPeriodicWorkPolicy.KEEP,
                request
        );
    }
}
