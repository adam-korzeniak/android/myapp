package com.adamkorzeniak.myapp.teide.ui;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class GlobalData {

    private GlobalData() {}
    private static LocalTime lastProcessStarted;
    private static LocalTime lastProcessFinished;

    public static LocalTime getLastProcessStarted() {
        return lastProcessStarted;
    }

    public static LocalTime getLastProcessFinished() {
        return lastProcessFinished;
    }

    public static void updateProcessStarted() {
        lastProcessStarted = getCurrentTime();
    }

    public static void updateProcessFinished() {
        lastProcessFinished = getCurrentTime();
    }

    private static LocalTime getCurrentTime() {
        return LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
    }
}
