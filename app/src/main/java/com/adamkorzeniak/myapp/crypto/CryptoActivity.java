package com.adamkorzeniak.myapp.crypto;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.adamkorzeniak.myapp.R;
import com.adamkorzeniak.myapp.common.android.persistence.AppDatabase;

public class CryptoActivity extends AppCompatActivity {

    private CryptoAssetDao cryptoAssetDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto);

        initDao();
        fetchCryptoAssets();
    }

    private void initDao() {
        AppDatabase database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, AppDatabase.DATABASE_NAME).build();
        this.cryptoAssetDao = database.getCryptoAssetDao();
    }

    private void fetchCryptoAssets() {
        cryptoAssetDao.findAll();
    }
}