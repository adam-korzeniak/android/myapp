package com.adamkorzeniak.myapp.crypto;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Collection;
import java.util.List;

@Dao
public interface CryptoAssetDao {

    @Query("SELECT * FROM cryptoAssetEntity")
    List<CryptoAssetEntity> findAll();

    @Query("SELECT * FROM cryptoAssetEntity WHERE id = :userId")
    CryptoAssetEntity findAllById(Long userId);

    @Query("SELECT * FROM cryptoAssetEntity WHERE id IN (:userIds)")
    List<CryptoAssetEntity> findAllByIds(Collection<Long> userIds);

    @Query("SELECT * FROM cryptoAssetEntity " +
            "WHERE symbol LIKE :symbol ")
    CryptoAssetEntity findBySymbol(String symbol);

    @Insert
    void insertAll(Collection<CryptoAssetEntity> cryptoAssets);

    @Update
    void saveAll(Collection<CryptoAssetEntity> cryptoAssets);

    @Delete
    void delete(CryptoAssetEntity cryptoAsset);
}