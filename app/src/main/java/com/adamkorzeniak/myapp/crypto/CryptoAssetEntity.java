package com.adamkorzeniak.myapp.crypto;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CryptoAssetEntity {

    @PrimaryKey
    public long id;

    @ColumnInfo(name = "symbol")
    public String symbol;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "amount")
    public Double amount;
}
